#!/usr/bin/env python3

from io import StringIO

from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

	def test_solve_1(self):
		inp = StringIO("A PearlHarbor Hold\nB Tokyo Move PearlHarbor\nC Osaka Move PearlHarbor\n")
		outp = "A [dead]\nB [dead]\nC [dead]\n"
		w = StringIO()
		diplomacy_solve(inp, w)
		sol = w.getvalue()
		self.assertEqual(sol, outp)

	def test_solve_2(self):
		inp = StringIO("A Paris Hold\nB London Support A\nC Berlin Move London\nD Vienna Move Paris\nE Rome Support D\n")
		outp = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Rome\n"
		w = StringIO()
		diplomacy_solve(inp, w)
		sol = w.getvalue()
		self.assertEqual(sol, outp)

	def test_solve_3(self):
		inp = StringIO("A Nagasaki Support B\nB Hiroshima Support A\nC Manhattan Move Nagasaki\nD Washington Move Hiroshima\n")
		outp = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
		w = StringIO()
		diplomacy_solve(inp, w)
		sol = w.getvalue()
		self.assertEqual(sol, outp)

if __name__ == '__main__':
	main()

#pragma: no cover
